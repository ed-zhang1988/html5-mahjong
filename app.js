var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));
var http = require('http').Server(app);
var io = require('socket.io')(http);

var mahjong = require('./mahjong');

io.on('connection', function(socket){
  console.log('a user connected');

  socket.on('shuffle', function(){
    socket['deck'] = mahjong.shuffle();
    socket.emit('shuffle', {
      base: socket['deck'].draw(13),
      newTile: socket['deck'].draw(),
      remaining: socket['deck'].length
    });
  });

  socket.on('new tile', function(seq){
    socket.emit('new tile', {
      newTile: socket['deck'].draw(),
      remaining: socket['deck'].length
    });
  });

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(3469, function(){
  console.log('listening on *:3469');
});
