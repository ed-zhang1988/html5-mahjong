var allTiles = [];

var baseTileSufix = "\udc00";
function tileAt(offset) {
  return "\ud83c" + String.fromCharCode(baseTileSufix.charCodeAt() + offset);
}

for(var i = 7; i <= 40; i++) {
  var normalCount = 4, doraCount = 0;
  if(i == 11 || i == 20) {
    normalCount = 3;
    doraCount = 1;
  }
  if(i == 29) {
    normalCount = 2;
    doraCount = 2;
  }
  for(var j=0; j < normalCount; j++) {
    var tile = {
      charactor: tileAt(i % 34),
      dora: false
    };
    allTiles.push(tile);
    tile.seq = allTiles.length;
  }
  for(var j=0; j < doraCount; j++) {
    var tile = {
      charactor: tileAt(i % 34),
      dora: true
    };
    allTiles.push(tile);
    tile.seq = allTiles.length;
  }
}

var shuffle = require('shuffle');

module.exports = {
  shuffle: function() {
    return shuffle.shuffle({deck: allTiles});
  }
};